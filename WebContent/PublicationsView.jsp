<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<%@ include file = "Header.jsp"%>
		<title>Publication ${pub.id}</title>
	</head>
	<body>
		<table>
			<tr>
				<th>Id<td>${pub.id}</td></th>
			</tr>
			<tr>
				<th>Title<td>${pub.title}</td></th>
			</tr>
			<tr>
				<th>Publication name<td>${pub.publicationName}</td></th>
			</tr>
			<tr>
				<th>Publication date<td>${pub.publicationDate}</td></th>
			</tr>
			<tr>
				<th>Authors<td>${pub.authors}</td></th>
			</tr>
			<tr>
				<th>Cite count<td>${pub.citeCount}</td></th>
			</tr>
	        <tr>
	        </tr>
		</table>
	</body>
</html>