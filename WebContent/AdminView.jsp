<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<%@ include file = "Header.jsp"%>
		<title>Title</title>
	</head>
	<body>
		<form action="CreateResearcherServlet" method="post">
	        <input type="text" name="id" placeholder="User Id"> 
			<input type="text" name="name" placeholder="Name">
			<input type="text" name="lastname" placeholder="Last name">
			<input type="text" name="email" placeholder="Email">
			<input type="text" name="password" placeholder="Password">
			<button type="submit">Create researcher</button>
		</form>
		
		<form action="CreatePublicationServlet" method="post">
	        <input type="text" name="id" placeholder="Publication Id">
	        <input type="text" name="title" placeholder="Title">
	        <input type="text" name="publicationName" placeholder="Name of Journal">
	        <input type="text" name="publicationDate" placeholder="Publication Date">
	        <input type="text" name="authors" placeholder="Authors IDs">
	        <button type="submit">Create publication</button>
		</form>
	</body>
</html>