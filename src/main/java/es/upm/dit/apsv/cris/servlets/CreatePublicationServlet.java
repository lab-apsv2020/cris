package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;
import java.net.URI;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.jsonp.JsonProcessingFeature;

import es.upm.dit.apsv.cris.model.Publication;
import es.upm.dit.apsv.cris.model.Researcher;

/**
 * Servlet implementation class CreatePublicationServlet
 */
@WebServlet("/CreatePublicationServlet")
public class CreatePublicationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Researcher user = (Researcher) request.getSession().getAttribute("user");
	    // Root cannot create publications
		if (!user.getId().equals("root")) {
	        
	    	String id = request.getParameter("id");
			String title = request.getParameter("title");
			String publicationName = request.getParameter("publicationName");
			String publicationDate = request.getParameter("publicationDate");
			String authors = request.getParameter("authors");

			
	    	Publication p = new Publication();
	        p.setId(id);
	        p.setAuthors(authors);
	        p.setPublicationDate(publicationDate);
	        p.setPublicationName(publicationName);
	        p.setTitle(title);
	        
			Client client = ClientBuilder.newClient(new ClientConfig());
	        
			try {
				Publication ri = client.target(URLHelper.getInstance().getCrisURL() + "/rest/Publications/" 
				         + id)
		        		.request().accept(MediaType.APPLICATION_JSON).get(Publication.class);
				request.setAttribute("message", "There is a publication with this id already. " + ri);
			} catch (Exception e) {
				Response resp = client.register(JsonProcessingFeature.class)
			        	.target(URLHelper.getInstance().getCrisURL() + "/rest/Publications/")
			        	.request().post(Entity.entity(p, MediaType.APPLICATION_JSON),
			        			Response.class);
			        	URI uri = resp.getLocation();
			        	request.setAttribute("message", "Researcher created at: " + uri);
			        	request.setAttribute("id", user.getId());
			}
			
	        response.sendRedirect(request.getContextPath() + "/ResearcherServlet");
	        return;
	    }	    
	    else {
	      request.getSession().invalidate();
	      request.setAttribute("message", "Root cannot to perform this operation");
	      getServletContext().getRequestDispatcher("/LoginView.jsp").forward(request, response);
	    }
	}

}
