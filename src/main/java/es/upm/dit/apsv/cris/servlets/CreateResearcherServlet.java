package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;
import java.net.URI;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.jsonp.JsonProcessingFeature;

import es.upm.dit.apsv.cris.model.Researcher;

/**
 * Servlet implementation class CreateResearcherServlet
 */
@WebServlet("/CreateResearcherServlet")
public class CreateResearcherServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Researcher researcher = (Researcher) request.getSession().getAttribute("user");
	    if (researcher.getId().equals("root")) {
	        
	    	String id = request.getParameter("id");
			String name = request.getParameter("name");
			String lastname = request.getParameter("lastname");
			String email = request.getParameter("email");
			
	    	Researcher r = new Researcher();
	        r.setId(id);
	        r.setName(name);
	        r.setLastname(lastname);
	        r.setEmail(email);
	        
			Client client = ClientBuilder.newClient(new ClientConfig());
	        
			try {
				Researcher ri = client.target(URLHelper.getInstance().getCrisURL() + "/rest/Researchers/" 
				         + id)
		        		.request().accept(MediaType.APPLICATION_JSON).get(Researcher.class);
				request.setAttribute("message", "There is a researcher with this id already." + ri);
			} catch (Exception e) {
				Response resp = client.register(JsonProcessingFeature.class)
			        	.target(URLHelper.getInstance().getCrisURL() + "/rest/Researchers/")
			        	.request().post(Entity.entity(r, MediaType.APPLICATION_JSON),
			        			Response.class);
			        	URI uri = resp.getLocation();
			        	request.setAttribute("message", "Researcher created at: " + uri);
			}
			
	        response.sendRedirect(request.getContextPath() + "/AdminServlet");
	        return;
	    }	    
	    else {
	      request.getSession().invalidate();
	      request.setAttribute("message", "You are not allowed to perform this operation");
	      getServletContext().getRequestDispatcher("/LoginView.jsp").forward(request, response);
	    }
	}

}
